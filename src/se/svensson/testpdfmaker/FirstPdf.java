package se.svensson.testpdfmaker;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



  
public class FirstPdf {
	private static String currentMember;
	private static String FILE = "c:/java/";
	  private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
	      Font.BOLD);
	  private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
	      Font.NORMAL, BaseColor.RED);
	  private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
	      Font.BOLD);
	  private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
	      Font.BOLD);
	
	public static void main(String[] args) {
		
		String lohse = "Lohse";
		String svenssson = "Svensson";
		String istentes = "Istenes";
		
		ArrayList<String> members = new ArrayList<>();
		members.add(istentes);
		members.add(svenssson);
		members.add(lohse);
		
		for(String member : members ){
			currentMember = member;

		
		    try {
		      Document document = new Document();
		      PdfWriter.getInstance(document, new FileOutputStream(FILE + member+".pdf"));
		      document.open();
		      addMetaData(document);
		      addTitlePage(document);
		  //    addContent(document);
		      document.close();
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		  }
	}
		  // iText allows to add metadata to the PDF which can be viewed in your Adobe
		  // Reader
		  // under File -> Properties
		  private static void addMetaData(Document document) {
		    document.addTitle("My first PDF");
		    document.addSubject("Using iText");
		    document.addKeywords("Java, PDF, iText");
		    document.addAuthor("Lars Vogel");
		    document.addCreator("Lars Vogel");
		  }

		  private static void addTitlePage(Document document)
		      throws DocumentException {
		    Paragraph preface = new Paragraph();
		    // We add one empty line
		    addEmptyLine(preface, 1);
		  
		    addEmptyLine(preface, 3);
		    preface.add(new Paragraph(currentMember, subFont));
		    preface.add(new Paragraph("Hejv�gen 4", subFont));
		    preface.add(new Paragraph("555 55", subFont));
		    preface.add(new Paragraph("Power Ville", subFont));
		    
		    addEmptyLine(preface, 1);
	
		    addEmptyLine(preface, 3);
		    addEmptyLine(preface, 3);
		    addEmptyLine(preface, 3);
		    preface.add(new Paragraph("Familjeavgift: 4000 kr ",
		        smallBold));

		    addEmptyLine(preface, 8);

		    preface.add(new Paragraph("PDF-making is fun!",
		        redFont));

		    document.add(preface);


		  private static void addEmptyLine(Paragraph paragraph, int number) {
		    for (int i = 0; i < number; i++) {
		      paragraph.add(new Paragraph(" "));
		    }
		  }
		 
	
	}


